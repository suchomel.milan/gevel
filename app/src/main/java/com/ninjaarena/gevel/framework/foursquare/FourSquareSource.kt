package com.ninjaarena.gevel.framework.foursquare

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.ninjaarena.gevel.data.MarkerApi
import com.ninjaarena.gevel.data.MarkerFourSquare
import com.ninjaarena.gevel.framework.ApiSource
import com.ninjaarena.gevel.framework.foursquare.data.FoursquareJSON
import com.ninjaarena.gevel.framework.foursquare.data.FoursquareVenue
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

class FourSquareSource(context: Context) : ApiSource<FoursquareVenue, MarkerFourSquare>() {
    override fun loadDetail(markerDetail: MutableLiveData<MarkerApi>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        private const val API = "https://api.foursquare.com/v2/"
    }

    private val fourSquare: FourSquareApi

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(API)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        fourSquare = retrofit.create(FourSquareApi::class.java)
    }

    override fun search(location: LatLng) {
        val ll = "${location.latitude},${location.longitude}"
        val request = fourSquare.searchVenues(filter, ll)

        request.enqueue(object : Callback<FoursquareJSON> {
            override fun onResponse(call: Call<FoursquareJSON>, response: Response<FoursquareJSON>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        it.response?.let { tit ->
                            addToList(tit.venues.toList())
                        }
                    }
                } else {
                    Timber.e(response.message())
                }
            }

            override fun onFailure(call: Call<FoursquareJSON>, t: Throwable) {
                t.printStackTrace()
            }

        })
    }

    override fun FoursquareVenue.toMarker(): MarkerFourSquare = MarkerFourSquare(this)

}