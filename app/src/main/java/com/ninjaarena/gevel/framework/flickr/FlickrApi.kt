package com.ninjaarena.gevel.framework.flickr

import com.ninjaarena.gevel.framework.flickr.data.ResponsePhotosFlickr
import com.ninjaarena.gevel.framework.flickr.data.ResponseUserFlickr
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickrApi {

    companion object {
        private const val API_KEY = "cd4cd439c5614f63fb4d370e5d420fb6"
        private const val PRIVATE_SECRET = "afb75955752e9450"

        private const val PHOTOS_PER_PAGE = 250
        private const val RADIUS = 0.3

        private val EXTRAS = "geo,description,owner_name,date_upload,views"
    }

    @GET("?method=flickr.photos.search&format=json&nojsoncallback=1")
    fun searchPhotos(
        @Query("text") query: String?,
        @Query("lat") lat: Double,
        @Query("lon") lng: Double,
        @Query("radius") radius: Double = RADIUS,
        @Query("page") page: Int = 1,
        @Query("extras") extras: String = EXTRAS,
        @Query("per_page") perPage: Int = PHOTOS_PER_PAGE,
        @Query("api_key") version: String = API_KEY
    ): Call<ResponsePhotosFlickr>

    @GET("?method=flickr.people.getInfo&format=json&nojsoncallback=1")
    fun searchUser(
        @Query("user_id") query: String?,
        @Query("api_key") version: String = API_KEY
    ): Call<ResponseUserFlickr>

}