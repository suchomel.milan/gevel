package com.ninjaarena.gevel.framework.googlePlaces.data.nearby

data class Location(

    val lat: Double,
    val lng: Double
)