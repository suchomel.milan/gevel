package com.ninjaarena.gevel.framework.foursquare

import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.ninjaarena.gevel.data.MarkerApi
import com.ninjaarena.gevel.data.MarkerFourSquare
import com.ninjaarena.gevel.framework.ApiSource
import com.ninjaarena.gevel.framework.RetrofitCallback
import com.ninjaarena.gevel.framework.foursquare.data.ResponseVenueFoursquare
import com.ninjaarena.gevel.framework.foursquare.data.ResponseVenuesFoursquare
import com.ninjaarena.gevel.framework.foursquare.data.VenueFoursquare
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FoursquareSource : ApiSource<VenueFoursquare, MarkerFourSquare>() {

    companion object {
        private const val API = "https://api.foursquare.com/v2/"
    }

    private val fourSquare: FourSquareApi

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(API)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        fourSquare = retrofit.create(FourSquareApi::class.java)
    }

    override fun search(location: LatLng) {
        val ll = "${location.latitude},${location.longitude}"
        val request = fourSquare.searchVenues(filter, ll)

        request.enqueue(object : RetrofitCallback<ResponseVenuesFoursquare>() {
            override fun onSuccessfulResponse(response: ResponseVenuesFoursquare) {
                addToList(response.response.venues.toList())
            }
        })
    }

    override fun loadDetail(markerDetail: MutableLiveData<MarkerApi>) {
        val id = (markerDetail.value as MarkerFourSquare).venue.id

        val request = fourSquare.venueDetail(id)

        request.enqueue(object : RetrofitCallback<ResponseVenueFoursquare>() {
            override fun onSuccessfulResponse(response: ResponseVenueFoursquare) {
                if ((markerDetail.value as MarkerFourSquare).venue.id == response.response.venue.id)
                    response.response.venue.isDetail = true
                markerDetail.value = response.response.venue.toMarker()
            }
        })
    }

    override fun VenueFoursquare.toMarker(): MarkerFourSquare = MarkerFourSquare(this)

}