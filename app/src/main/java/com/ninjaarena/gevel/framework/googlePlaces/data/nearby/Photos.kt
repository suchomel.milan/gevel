package com.ninjaarena.gevel.framework.googlePlaces.data.nearby

data class Photos(

    val height: Int,
    val html_attributions: List<String>,
    val photo_reference: String,
    val width: Int
)