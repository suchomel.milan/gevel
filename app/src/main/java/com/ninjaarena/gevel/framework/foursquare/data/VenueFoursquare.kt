package com.ninjaarena.gevel.framework.foursquare.data

data class VenueFoursquare(
    val id: String,
    val name: String,
    val contact: Contact,
    val location: Location,
    val categories: ArrayList<Category>,
    val stats: Stats,
    val likes: Likes,
    val rating: Double,
    val ratingColor: String?,
    val tips: Tips,
    val hours: Hours,
    val bestPhoto: Photo?
) {

    var isDetail = false

    fun getCategoriesString(): String {
        var tmp = ""
        categories.forEach {
            if (tmp != "")
                tmp += ", "
            tmp += it.name
        }
        return tmp
    }


    data class Contact(
        val phone: String?,
        val formattedPhone: String?,
        val twitter: String?
    )

    data class Location(
        val lat: Double,
        val lng: Double,
        val formattedAddress: ArrayList<String>?
    )

    data class Category(
        val name: String
    )

    data class Stats(
        val tipCount: Int
    )

    data class Likes(
        val tipCount: Int
    )

    data class Tips(
        val groups: ArrayList<Group>
    )

    data class Group(
        val items: ArrayList<TipItem>
    )

    data class TipItem(
        val text: String,
        val agreeCount: Int,
        val disagreeCount: Int,
        val user: User
    )

    data class User(
        val id: String,
        val firstName: String,
        val lastName: String,
        val photo: Photo
    ) {
        fun getFullname() = "$firstName $lastName"
    }

    data class Photo(
        val prefix: String,
        val suffix: String,
        val width: Int,
        val height: Int
    ) {
        fun getPhotoUrl(): String {
            return "$prefix${if (width == 0) 100 else width}x${if (height == 0) 100 else height}$suffix"
        }
    }

    data class Hours(
        val status: String,
        val isOpen: Boolean
    )
}