package com.ninjaarena.gevel.framework.googlePlaces.data.nearby

data class Place(

    val geometry: Geometry,
    val icon: String,
    val id: String,
    val name: String,
    val opening_hours: OpeningHours,
    val photos: List<Photos>,
    val place_id: String,
    val scope: String,
    val alt_ids: List<AltId>,
    val reference: String,
    val types: List<String>,
    val vicinity: String
)