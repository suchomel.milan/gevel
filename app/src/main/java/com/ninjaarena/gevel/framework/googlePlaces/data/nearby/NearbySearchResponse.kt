package com.ninjaarena.gevel.framework.googlePlaces.data.nearby

data class NearbySearchResponse(

    val error_message: String?,
    val html_attributions: List<String>,
    val results: List<Place>,
    val status: String
)