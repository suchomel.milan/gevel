package com.ninjaarena.gevel.framework.flickr.data

class ResponseUserFlickr private constructor(
    private val person: Person
) {
    val id get() = person.id
    val iconUrl
        get() =
            if (person.iconserver == "0")
                "https://s.yimg.com/pw/images/buddyicon11_r.png"
            else
                "https://farm${person.iconfarm}.staticflickr.com/${person.iconserver}/buddyicons/${person.id}.jpg"
    val username get() = person.username._content
    val realname get() = person.realname?._content

    data class Person(
        val id: String,
        val iconserver: String,
        val iconfarm: String,
        val username: Content,
        val realname: Content?
    )

    data class Content(
        val _content: String
    )
}

