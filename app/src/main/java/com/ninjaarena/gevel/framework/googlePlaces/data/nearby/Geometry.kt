package com.ninjaarena.gevel.framework.googlePlaces.data.nearby

data class Geometry(

    val location: Location
)