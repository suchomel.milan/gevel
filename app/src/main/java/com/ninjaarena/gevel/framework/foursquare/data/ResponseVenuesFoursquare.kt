package com.ninjaarena.gevel.framework.foursquare.data

import java.util.*

data class ResponseVenuesFoursquare(
    val response: Response
) {
    data class Response(
        val venues: ArrayList<VenueFoursquare> = ArrayList()
    )
}
