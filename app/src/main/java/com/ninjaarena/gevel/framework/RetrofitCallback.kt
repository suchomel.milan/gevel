package com.ninjaarena.gevel.framework

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

abstract class RetrofitCallback<E> : Callback<E> {

    override fun onFailure(call: Call<E>, t: Throwable) {
        t.printStackTrace()
    }

    override fun onResponse(call: Call<E>, response: Response<E>) {
        if (response.isSuccessful) {
            response.body()?.let {
                Timber.d("Success - %s", call.request().url())
                onSuccessfulResponse(it)
            }
            if (response.body() == null) {
                Timber.e("Response body is null")
            }
        } else {
            Timber.e(response.message())
        }
    }

    abstract fun onSuccessfulResponse(response: E)
}