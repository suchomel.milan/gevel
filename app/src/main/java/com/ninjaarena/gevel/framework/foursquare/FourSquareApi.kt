package com.ninjaarena.gevel.framework.foursquare

import com.ninjaarena.gevel.framework.foursquare.data.ResponseVenueFoursquare
import com.ninjaarena.gevel.framework.foursquare.data.ResponseVenuesFoursquare
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FourSquareApi {

    companion object {
        private const val VERSION = "26032019"
        private const val CLIENT_ID = "SFEU2KFVY301JYMAOCRB1FF2DD3YQUJUJSQFDX0WLTCWALEL"
        private const val CLIENT_SECRET = "PTRC4FIVU2IKC33NNVGQK4RLCQM0MWXH4DI1531KWWXOWAM4"
    }

    @GET("venues/search")
    fun searchVenues(
        @Query("query") query: String?,
        @Query("ll") ll: String,
        @Query("venuePhotos") venuePhotos: Int = 1,
        @Query("v") version: String = VERSION,
        @Query("client_id") clientID: String = CLIENT_ID,
        @Query("client_secret") clientSecret: String = CLIENT_SECRET
    ): Call<ResponseVenuesFoursquare>

    @GET("venues/{id}")
    fun venueDetail(
        @Path("id") id: String?,
        @Query("v") version: String = VERSION,
        @Query("client_id") clientID: String = CLIENT_ID,
        @Query("client_secret") clientSecret: String = CLIENT_SECRET
    ): Call<ResponseVenueFoursquare>
}