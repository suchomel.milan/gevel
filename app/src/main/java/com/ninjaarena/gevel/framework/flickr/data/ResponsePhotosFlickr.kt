package com.ninjaarena.gevel.framework.flickr.data

data class ResponsePhotosFlickr(
    val photos: Photos,
    val stat: String?
) {
    data class Photos(
        val page: Number?,
        val pages: Number?,
        val perpage: Number?,
        val total: String?,
        val photo: List<PhotoFlickr> = arrayListOf()
    )
}

