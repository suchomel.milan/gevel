package com.ninjaarena.gevel.framework

import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.ninjaarena.gevel.data.MarkerApi

interface IApiSource {

    val livedata: MutableLiveData<ArrayList<MarkerApi>>

    fun getData(): ArrayList<MarkerApi>
    fun search(location: LatLng, filter:String? = null)
    fun loadDetail(markerDetail: MutableLiveData<MarkerApi>)

}