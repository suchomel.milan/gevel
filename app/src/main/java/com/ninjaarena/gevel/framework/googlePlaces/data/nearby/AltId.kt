package com.ninjaarena.gevel.framework.googlePlaces.data.nearby

data class AltId(

    val place_id: String,
    val scope: String
)