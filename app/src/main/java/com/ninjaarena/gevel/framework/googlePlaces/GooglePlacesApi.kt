package com.ninjaarena.gevel.framework.googlePlaces

import com.ninjaarena.gevel.framework.googlePlaces.data.nearby.NearbySearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GooglePlacesApi {

    companion object {
        private const val API_KEY = "AIzaSyDLM0xm59BhdsY7XM8IYhI_-nuckF5p_po"
    }

    @GET("nearbysearch/json")
    fun nearbySearch(
        @Query("location") location: String,
        @Query("radius") radius: Int,
        @Query("key") apiKey: String = API_KEY
    ): Call<NearbySearchResponse>

    @GET("textsearch/json")
    fun textSearch(
        @Query("query") query: String,
        @Query("location") location: String,
        @Query("radius") radius: Int,
        @Query("key") apiKey: String = API_KEY
    ): Call<NearbySearchResponse>
}