package com.ninjaarena.gevel.framework.flickr.data

class PhotoFlickr(
    val id: String,
    val owner: String?,
    val secret: String?,
    val server: String?,
    val farm: Number?,
    val title: String?,
    val ispublic: Number?,
    val isfriend: Number?,
    val isfamily: Number?,
    val description: ResponseUserFlickr.Content?,
    val dateupload: String?,
    val ownername: String?,
    val views: String?,
    val latitude: String,
    val longitude: String,
    val accuracy: String?,
    val context: Number?,
    val place_id: String?,
    val woeid: String?,
    val geo_is_family: Number?,
    val geo_is_friend: Number?,
    val geo_is_contact: Number?,
    val geo_is_public: Number?
) {

    companion object {
        private const val SQUARE_LARGE_IMAGE_SUFFIX = "_q.jpg"
        private const val MEDIUM_IMAGE_SUFFIX = ".jpg"
    }

    var user: ResponseUserFlickr? = null

    private fun _getBaseImageUrl(): StringBuffer {
        val buffer = StringBuffer()
        buffer.append("https://farm")
        buffer.append(farm)
        buffer.append(".static.flickr.com/")
        buffer.append(server)
        buffer.append("/")
        buffer.append(id)
        buffer.append("_")
        return buffer
    }

    private fun getBaseImageUrl(): StringBuffer {
        val buffer = StringBuffer()
        buffer.append(_getBaseImageUrl())
        buffer.append(secret)
        return buffer
    }

    fun getUserIconUrl(): String {
        return user?.iconUrl ?: "https://s.yimg.com/pw/images/buddyicon11_r.png#168737283@N04"
    }

    fun getSquareLargeUrl(): String = getBaseImageUrl().append(SQUARE_LARGE_IMAGE_SUFFIX).toString()

    fun getMediuUrl(): String = getBaseImageUrl().append(MEDIUM_IMAGE_SUFFIX).toString()
}