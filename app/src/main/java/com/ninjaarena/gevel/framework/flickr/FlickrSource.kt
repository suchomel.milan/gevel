package com.ninjaarena.gevel.framework.flickr


import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.ninjaarena.gevel.data.MarkerApi
import com.ninjaarena.gevel.data.MarkerFlickr
import com.ninjaarena.gevel.framework.ApiSource
import com.ninjaarena.gevel.framework.RetrofitCallback
import com.ninjaarena.gevel.framework.flickr.data.PhotoFlickr
import com.ninjaarena.gevel.framework.flickr.data.ResponsePhotosFlickr
import com.ninjaarena.gevel.framework.flickr.data.ResponseUserFlickr
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FlickrSource : ApiSource<PhotoFlickr, MarkerFlickr>() {

    companion object {
        private const val API = "https://api.flickr.com/services/rest/"
    }

    private val flickr: FlickrApi

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(API)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        flickr = retrofit.create(FlickrApi::class.java)
    }

    override fun search(location: LatLng) {
        val request = flickr.searchPhotos(filter, location.latitude, location.longitude)

        request.enqueue(object : RetrofitCallback<ResponsePhotosFlickr>() {
            override fun onSuccessfulResponse(response: ResponsePhotosFlickr) {
                addToList(response.photos.photo.toList())
            }
        })
    }

    override fun loadDetail(markerDetail: MutableLiveData<MarkerApi>) {
        val userId = (markerDetail.value as MarkerFlickr).photo.owner

        val request = flickr.searchUser(userId)

        request.enqueue(object : RetrofitCallback<ResponseUserFlickr>() {
            override fun onSuccessfulResponse(response: ResponseUserFlickr) {
                val photo = (markerDetail.value as MarkerFlickr).photo
                if (photo.owner == response.id) {
                    photo.user = response
                    markerDetail.value = photo.toMarker()
                }
            }
        })
    }

    override fun PhotoFlickr.toMarker(): MarkerFlickr = MarkerFlickr(this)
}