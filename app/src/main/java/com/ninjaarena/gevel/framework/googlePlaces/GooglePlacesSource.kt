package com.ninjaarena.gevel.framework.googlePlaces

import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.ninjaarena.gevel.data.MarkerApi
import com.ninjaarena.gevel.data.MarkerGooglePlaces
import com.ninjaarena.gevel.framework.ApiSource
import com.ninjaarena.gevel.framework.RetrofitCallback
import com.ninjaarena.gevel.framework.googlePlaces.data.nearby.NearbySearchResponse
import com.ninjaarena.gevel.framework.googlePlaces.data.nearby.Place
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class GooglePlacesSource : ApiSource<Place, MarkerGooglePlaces>() {
    override fun loadDetail(markerDetail: MutableLiveData<MarkerApi>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        const val API = "https://maps.googleapis.com/maps/api/place/"
        const val API_KEY = "AIzaSyDLM0xm59BhdsY7XM8IYhI_-nuckF5p_po"

        fun getPhotoUrl(photoRef: String): String {
            return "${API}photo?maxwidth=400&photoreference=$photoRef&key=$API_KEY"
        }
    }

    private val googlePlaces: GooglePlacesApi

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(API)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        googlePlaces = retrofit.create(GooglePlacesApi::class.java)
    }

    override fun Place.toMarker(): MarkerGooglePlaces = MarkerGooglePlaces(this)

    override fun search(location: LatLng) {
        val ll = "${location.latitude},${location.longitude}"
        val tmpFilter = filter
        val request = if (tmpFilter == null)
            googlePlaces.nearbySearch(ll, 500)
        else
            googlePlaces.textSearch(tmpFilter, ll, 500)

        request.enqueue(object : RetrofitCallback<NearbySearchResponse>() {
            override fun onSuccessfulResponse(response: NearbySearchResponse) {
                addToList(response.results.toList())
            }
        })
    }
}