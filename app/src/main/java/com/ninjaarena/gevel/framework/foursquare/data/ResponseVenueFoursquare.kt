package com.ninjaarena.gevel.framework.foursquare.data

data class ResponseVenueFoursquare(
    val response: Response
) {
    data class Response(
        val venue: VenueFoursquare
    )
}