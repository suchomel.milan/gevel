package com.ninjaarena.gevel.framework.googlePlaces.data.nearby

data class OpeningHours(

    val open_now: Boolean
)