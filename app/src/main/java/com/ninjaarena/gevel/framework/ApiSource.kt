package com.ninjaarena.gevel.framework

import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.ninjaarena.gevel.data.MarkerApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.locks.ReadWriteLock
import java.util.concurrent.locks.ReentrantReadWriteLock

abstract class ApiSource<E, T : MarkerApi> : IApiSource {

    final override val livedata = MutableLiveData<ArrayList<MarkerApi>>()
    var filter: String? = null
    val api = javaClass.simpleName
    private val readWriteLock: ReadWriteLock = ReentrantReadWriteLock()

    init {
        livedata.value = arrayListOf()
    }

    final override fun search(location: LatLng, filter: String?){
        if(this.filter != filter){
            livedata.value?.clear()
            this.filter = filter
        }
        search(location)
    }

    abstract fun search(location: LatLng)

    override fun getData(): ArrayList<MarkerApi> {
        return livedata.value ?: arrayListOf()
    }

    protected fun addToList(list: ArrayList<MarkerApi>) {
        readWriteLock.writeLock().lock()
        val source = arrayListOf<MarkerApi>()
        source.addAll(livedata.value ?: arrayListOf())

        val size = source.size
        for (it in list) {
            if (!source.contains(it)) {
                source.add(it)
            }
        }

        Timber.d(
            "%s data changed: [PRE: %d, POST: %d, NEW: %d(%d)]",
            api,
            size,
            source.size,
            source.size - size,
            list.size
        )

        GlobalScope.launch(Dispatchers.Main) {
            livedata.value = source
        }
        readWriteLock.writeLock().unlock()
    }

    abstract fun E.toMarker(): T

    protected fun List<E>.toList(): ArrayList<MarkerApi> {
        val list = arrayListOf<MarkerApi>()

        for (it in this) {
            list.add(it.toMarker())
        }

        return list
    }
}