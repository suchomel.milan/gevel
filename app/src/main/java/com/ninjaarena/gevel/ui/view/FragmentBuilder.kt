package com.ninjaarena.gevel.ui.view

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.ninjaarena.gevel.R
import com.ninjaarena.gevel.data.MarkerApi
import com.ninjaarena.gevel.data.MarkerFlickr
import com.ninjaarena.gevel.data.MarkerFourSquare
import com.ninjaarena.gevel.data.MarkerGooglePlaces
import com.ninjaarena.gevel.modules.GlideApp

class FragmentBuilder(layoutInflater: LayoutInflater, root: ViewGroup, item: MarkerApi) {

    private val view: View

    private val viewResourceId =
        when (item) {
            is MarkerFlickr -> R.layout.detail_flickr
            is MarkerGooglePlaces -> R.layout.detail_foursquare
            is MarkerFourSquare -> R.layout.detail_foursquare
            else -> throw ClassNotFoundException()
        }

    init {
        root.removeAllViews()
        val frameLayout = layoutInflater.inflate(R.layout.detail_frame_layout, root, true)
        view = layoutInflater.inflate(viewResourceId, frameLayout.findViewById(R.id.content_layout) as ViewGroup, true)
        fillContent(item)
    }

    fun fillContent(marker: MarkerApi) {
        when (marker) {
            is MarkerFlickr -> fillContent(marker)
            is MarkerGooglePlaces -> fillContent(marker)
            is MarkerFourSquare -> fillContent(marker)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun fillContent(marker: MarkerFlickr) {
        val userIcon = view.findViewById<ImageView>(R.id.user_icon)
        val username = view.findViewById<TextView>(R.id.user_name)
        val realname = view.findViewById<TextView>(R.id.tag_content)

        val image = view.findViewById<ImageView>(R.id.image_content)

        val title = view.findViewById<TextView>(R.id.content_title)
        val description = view.findViewById<TextView>(R.id.description)
        val views = view.findViewById<TextView>(R.id.info_views)

        username.text = marker.photo.ownername
        realname.visibility = if (marker.photo.user?.realname == null) View.GONE else View.VISIBLE
        realname.text = marker.photo.user?.realname
        title.text = marker.photo.title
        title.visibility = if (title.text.isEmpty()) View.GONE else View.VISIBLE
        description.text = marker.photo.description?._content
        description.visibility = if (description.text.isEmpty()) View.GONE else View.VISIBLE
        views.text = "${marker.photo.views} Views"

        GlideApp.with(view.context)
            .load(marker.photo.getUserIconUrl())
            .transform(CircleCrop())
            .into(userIcon)

        GlideApp.with(view.context)
            .load(marker.photo.getMediuUrl())
            .into(image)
    }

    private fun fillContent(marker: MarkerGooglePlaces) {

    }

    private fun fillContent(marker: MarkerFourSquare) {
        view.findViewById<TextView>(R.id.name).text = marker.venue.name
        view.findViewById<TextView>(R.id.categories).text = marker.venue.getCategoriesString()

        if (!marker.venue.isDetail) return

        val rating = view.findViewById<TextView>(R.id.rating)
        if (marker.venue.rating > 0) {
            rating.visibility = View.VISIBLE
            rating.text = marker.venue.rating.toString()
            rating.backgroundTintList =
                ColorStateList.valueOf(Color.parseColor("#${marker.venue.ratingColor ?: "666666"}"))
        } else
            rating.visibility = View.GONE

        marker.venue.bestPhoto?.let {
            GlideApp.with(view.context)
                .load(it.getPhotoUrl())
                .into(view.findViewById(R.id.image_content))
        }

        val phone = marker.venue.contact.formattedPhone ?: marker.venue.contact.phone
        val phoneView = view.findViewById<TextView>(R.id.phone_text)
        phoneView.text = phone
        (phoneView.parent as View).visibility = if (phone == null) View.GONE else View.VISIBLE

        val tipsView = view.findViewById<LinearLayout>(R.id.tips_layout)
        if (marker.venue.tips.groups.size > 0 && marker.venue.tips.groups[0].items.size > 0) {
            view.findViewById<TextView>(R.id.username).text = marker.venue.tips.groups[0].items[0].user.getFullname()
            view.findViewById<TextView>(R.id.tip_text).text = marker.venue.tips.groups[0].items[0].text

            GlideApp.with(view.context)
                .load(marker.venue.tips.groups[0].items[0].user.photo.getPhotoUrl())
                .transform(RoundedCorners(10))
                .into(view.findViewById(R.id.user_icon))
            tipsView.visibility = View.VISIBLE
        } else
            tipsView.visibility = View.GONE


    }
}