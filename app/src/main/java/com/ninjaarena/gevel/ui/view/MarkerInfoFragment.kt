package com.ninjaarena.gevel.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ninjaarena.gevel.R
import com.ninjaarena.gevel.data.MarkerApi


class MarkerInfoFragment : BottomSheetDialogFragment() {

    companion object {

        private const val TAG = "marker_info_bottom_sheet"
        private const val ARG_MARKER = "marker"

        fun getBottomSheetDialog(supportFragmentManager: FragmentManager, marker: MarkerApi) {
            var dialog = supportFragmentManager.findFragmentByTag(TAG) as MarkerInfoFragment?
            val exists = dialog != null

            if (!exists)
                dialog = MarkerInfoFragment()

            dialog!!.marker = marker

            if (!exists)
                dialog.show(supportFragmentManager, TAG)
        }
    }

    var marker: MarkerApi? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.bottom_sheet_single, container, false)



        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)


    }

    override fun onStart() {
        super.onStart()

        dialog?.window?.setDimAmount(0.0f)
        var view = dialog?.window?.decorView?.findViewById<View>(com.google.android.material.R.id.touch_outside)
        view?.isClickable = false
        view?.setOnClickListener(null)

        for (i in 0..5) {
            view = view?.parent as View?
            view?.isClickable = false
            view?.setOnClickListener(null)
        }
    }
}