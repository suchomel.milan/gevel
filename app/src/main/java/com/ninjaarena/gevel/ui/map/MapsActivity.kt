package com.ninjaarena.gevel.ui.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.maps.android.clustering.ClusterManager
import com.ninjaarena.gevel.R
import com.ninjaarena.gevel.data.EApiName
import com.ninjaarena.gevel.data.MarkerApi
import com.ninjaarena.gevel.extensions.toLatLng
import com.ninjaarena.gevel.ui.ClusterRenderer
import com.ninjaarena.gevel.ui.view.BottomSheetBuilder
import com.ninjaarena.gevel.ui.view.FragmentBuilder
import io.nlopez.smartlocation.OnLocationUpdatedListener
import io.nlopez.smartlocation.SmartLocation
import io.nlopez.smartlocation.location.config.LocationParams
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.button_current_location.*
import kotlinx.android.synthetic.main.button_search_places.*
import kotlinx.android.synthetic.main.map_toolbar.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import java.util.concurrent.locks.ReadWriteLock
import java.util.concurrent.locks.ReentrantReadWriteLock


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, OnLocationUpdatedListener,
    GoogleMap.OnCameraIdleListener, GestureDetector.OnGestureListener {

    companion object {
        private const val REQUEST_CODE = 666
    }

    private val viewModel: MapViewModel by viewModel()

    private lateinit var map: GoogleMap

    private lateinit var clusterManager: ClusterManager<MarkerApi>
    private lateinit var clusterRenderer: ClusterRenderer

    private var gestureScanner: GestureDetector? = null
    private val readWriteLock: ReadWriteLock = ReentrantReadWriteLock()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE)

        initObservers()
        initViews()
    }

    private fun initObservers() {
        // Track position
        viewModel.trackPosition.observe(this, Observer {
            val trackPosition = it == true
            current_location.getChildAt(0).isSelected = trackPosition
            if (::map.isInitialized)
                map.uiSettings.isScrollGesturesEnabled = !trackPosition
        })

        // API Filters
        viewModel.sources.forEach { apiName, api ->
            api.enabled.observe(this, Observer {
                apiFilterSetSelection(findViewById<FrameLayout>(apiName.resourceId), it)
            })
        }

        // Places list
        viewModel.bottomInfo.observe(this, Observer {
            showMarkersInfo(it.second[0])
        })

        // Markers
        viewModel.liveData.observe(this, Observer { list ->
            list?.let { showMarkers(it) }
        })


        for (source in viewModel.sources) {

            source.value.livedata.observe(this, Observer {
                viewModel.updatePlaces()
            })
        }
    }

    private fun initViews() {
        current_location.setOnClickListener {
            viewModel.trackPosition.value = false
            val location = SmartLocation.with(this).location().lastLocation?.toLatLng()
            if (location != null)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 17f))
            else
                Toast.makeText(this, "No current position", Toast.LENGTH_SHORT).show()
        }

        current_location.setOnLongClickListener {
            viewModel.trackPosition.value = true
            true
        }

        search_places.setOnClickListener {
            viewModel.searchPlaces(map.cameraPosition.target)
        }

        EApiName.values().forEach {
            findViewById<FrameLayout>(it.resourceId).setOnLongClickListener { v -> onLongClickApiFilter(v) }
        }

        gestureScanner = GestureDetector(this, this)
        info_content.setOnTouchListener { _, event ->
            gestureScanner?.onTouchEvent(event)!!
        }

        button_right_toolbar.setOnClickListener {
            setDialogFragmentVisible(false)
            val isSearching = !button_right_toolbar.isActivated
            button_right_toolbar.isActivated = isSearching

            button_right_toolbar.setImageResource(if (isSearching) R.drawable.ic_close else R.drawable.ic_search)
            logo_app.visibility = if (isSearching) View.GONE else View.VISIBLE
            edit_search.visibility = if (isSearching) View.VISIBLE else View.GONE

            edit_search.text = null

            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

            if (!isSearching) {
                inputMethodManager.hideSoftInputFromWindow(edit_search.windowToken, 0)
                viewModel.searchPlaces(map.cameraPosition.target)
            } else {
                edit_search.requestFocus()
                inputMethodManager.showSoftInput(edit_search, InputMethodManager.SHOW_IMPLICIT)
            }
        }

        edit_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.searchFilter.value = s?.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        edit_search.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.searchPlaces(map.cameraPosition.target)
                false
            } else
                false
        }
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
        if (velocityY > 4000 && Math.abs(velocityY) / 2 > Math.abs(velocityX))
            setDialogFragmentVisible(false)
        return true
    }

    override fun onBackPressed() {
        if (info_content.visibility == View.VISIBLE || detail_content.visibility == View.VISIBLE)
            setDialogFragmentVisible(false)
    }

    fun onClickToolbarLeft(view: View) {
        setDialogFragmentVisible(false)
    }

    private fun setDialogFragmentVisible(visible: Boolean) {
        if (!visible) {
            info_content.visibility = View.GONE
            detail_content.visibility = View.GONE
            button_left_toolbar.setImageResource(R.drawable.ic_menu)
        } else {
            info_content.visibility = View.VISIBLE
            button_left_toolbar.setImageResource(R.drawable.ic_arrow_back)
        }
    }

    private fun showMarkersInfo(marker: MarkerApi, animate: Boolean = false) {
        BottomSheetBuilder(layoutInflater, info_content, arrayListOf(marker)) { showMarkerDetail(it) }
        setDialogFragmentVisible(true)
    }

    private fun showMarkerDetail(marker: MarkerApi) {
        val builder = FragmentBuilder(layoutInflater, detail_content, marker)
        viewModel.markerDetail.value = marker
        viewModel.markerDetail.observe(this, Observer {
            builder.fillContent(it)
        })
        viewModel.updateDetail(marker)
        info_content.visibility = View.GONE
        detail_content.visibility = View.VISIBLE
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && ::map.isInitialized) {
                map.isMyLocationEnabled = true
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        clusterManager = ClusterManager(this, map)
        clusterRenderer = ClusterRenderer(this, map, clusterManager)
        clusterManager.setOnClusterItemClickListener { marker ->
            showMarkersInfo(marker, true)
            true
        }

        map.mapType = GoogleMap.MAP_TYPE_NORMAL
        map.isBuildingsEnabled = false
        map.uiSettings.isMapToolbarEnabled = false
        map.uiSettings.isMyLocationButtonEnabled = false
        map.uiSettings.isCompassEnabled = false
        map.uiSettings.isScrollGesturesEnabled = viewModel.trackPosition.value != true
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            map.isMyLocationEnabled = true
        }

        map.setOnCameraIdleListener(this)
        map.setOnMarkerClickListener(clusterManager)

        SmartLocation.with(this).location().config(LocationParams.NAVIGATION).start(this)
    }

    override fun onLocationUpdated(location: Location?) {
        if (viewModel.trackPosition.value == true) {
            location?.let { map.animateCamera(CameraUpdateFactory.newLatLng(it.toLatLng())) }
        }
    }

    private fun showMarkers(list: ArrayList<MarkerApi>) {
        if (!::clusterManager.isInitialized) return

        readWriteLock.writeLock().lock()
        try {
            clusterManager.clearItems()
            clusterManager.addItems(list)
        } catch (ex: Exception) {


        } finally {
            readWriteLock.writeLock().unlock()
        }
        clusterManager.cluster()
    }

    override fun onCameraIdle() {
        clusterManager.onCameraIdle()
    }

    fun onClickApiFilter(view: View) {
        viewModel.sources[EApiName.valueOfResource(view.id)]?.setSelection(!view.isSelected)
        viewModel.searchPlaces(map.cameraPosition.target)
    }

    private fun onLongClickApiFilter(view: View): Boolean {
        viewModel.sources.forEach { apiName, api -> api.enabled.value = apiName.resourceId == view.id }
        viewModel.searchPlaces(map.cameraPosition.target)
        return true
    }

    private fun apiFilterSetSelection(view: View, selected: Boolean?) {
        view.isSelected = selected == true
        val imageView = (view as FrameLayout).getChildAt(0) as ImageView
        imageView.alpha = if (view.isSelected) 1f else 0.5f
    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean = false
    override fun onDown(e: MotionEvent?): Boolean = false
    override fun onShowPress(e: MotionEvent?) {}
    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean = false
    override fun onLongPress(e: MotionEvent?) {}
}
