package com.ninjaarena.gevel.ui.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.ninjaarena.gevel.R
import com.ninjaarena.gevel.data.EApiName.*
import com.ninjaarena.gevel.data.MarkerApi
import com.ninjaarena.gevel.data.MarkerFlickr
import com.ninjaarena.gevel.data.MarkerFourSquare
import com.ninjaarena.gevel.data.MarkerGooglePlaces
import com.ninjaarena.gevel.framework.googlePlaces.GooglePlacesSource
import com.ninjaarena.gevel.modules.GlideApp

class BottomSheetBuilder(
    val layoutInflater: LayoutInflater,
    root: ViewGroup,
    val list: ArrayList<MarkerApi>,
    val callback: (MarkerApi) -> Unit
) {

    private val isSingle: Boolean
        get() {
            return list.size == 1
        }

    private val bottomSheetResourceId: Int
        get() {
            return if (isSingle) R.layout.bottom_sheet_single else R.layout.bottom_sheet_single
        }

    init {
        root.removeAllViews()
        val bottomSheet = layoutInflater.inflate(bottomSheetResourceId, root, true)
        inflateContent(bottomSheet as ViewGroup)
    }

    private fun inflateContent(parent: ViewGroup) {
        if (isSingle) inflateContentSingle(parent) else inflateContentMulti(parent)
    }

    private fun inflateContentSingle(parent: ViewGroup) {
        val view = layoutInflater.inflate(getContentResourceId(list.first()), parent, true)
        fillContent(view, list.first())
        view.setOnClickListener {
            callback.invoke(list.first())
        }
    }

    private fun inflateContentMulti(view: View) {}

    private fun getContentResourceId(marker: MarkerApi): Int {
        return when (marker.api) {
            FLICKR -> R.layout.list_item_flickr
            GOOGLE_PLACES -> R.layout.list_item_flickr
            FOUR_SQUARE -> R.layout.list_item_flickr
        }
    }

    private fun fillContent(view: View, marker: MarkerApi) {
        when (marker.api) {
            FLICKR -> fillContent(view, marker as MarkerFlickr)
            GOOGLE_PLACES -> fillContent(view, marker as MarkerGooglePlaces)
            FOUR_SQUARE -> fillContent(view, marker as MarkerFourSquare)
        }
    }

    private fun fillContent(view: View, marker: MarkerFlickr) {
        val title = view.findViewById<TextView>(R.id.title)
        val username = view.findViewById<TextView>(R.id.username)
        val stats = view.findViewById<TextView>(R.id.stats)
        val image = view.findViewById<ImageView>(R.id.image_thumbnail)
        val apiImage = view.findViewById<ImageView>(R.id.api_image)

        username.text = marker.photo.ownername
        title.text = marker.photo.title
        stats.text = "${marker.photo.views} Views"
        apiImage.setImageResource(R.drawable.flickr_logo)

        GlideApp.with(view.context)
            .load(marker.photo.getSquareLargeUrl())
            .transform(RoundedCorners(10))
            .into(image)
    }

    private fun fillContent(view: View, marker: MarkerGooglePlaces) {
        val username = view.findViewById<TextView>(R.id.username)
        val title = view.findViewById<TextView>(R.id.title)
        val stats = view.findViewById<TextView>(R.id.stats)
        val image = view.findViewById<ImageView>(R.id.image_thumbnail)
        val apiImage = view.findViewById<ImageView>(R.id.api_image)

        username.text = marker.place.name

        apiImage.setImageResource(R.drawable.google_places_logo)

        if (!marker.place.photos.isNullOrEmpty())
            GlideApp.with(view.context)
                .load(GooglePlacesSource.getPhotoUrl(marker.place.photos[0].photo_reference))
                .transform(MultiTransformation(CenterCrop(), RoundedCorners(10)))
                .into(image)
    }

    private fun fillContent(view: View, marker: MarkerFourSquare) {
        val username = view.findViewById<TextView>(R.id.username)
        val title = view.findViewById<TextView>(R.id.title)
        val stats = view.findViewById<TextView>(R.id.stats)
        val image = view.findViewById<ImageView>(R.id.image_thumbnail)
        val apiImage = view.findViewById<ImageView>(R.id.api_image)

        username.text = marker.venue.name

        var categories = ""
        for (category in marker.venue.categories) {
            if (categories.isNotEmpty())
                categories += ", "
            categories += category.name
        }

        stats.text = categories
        if (marker.venue.location.formattedAddress != null)
        //title.text = "${marker.venue.location.formattedAddress[0]}\n${marker.venue.location.formattedAddress[1]}"
        apiImage.setImageResource(R.drawable.foursquare_logo)
    }
}