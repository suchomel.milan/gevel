package com.ninjaarena.gevel.ui.map

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.ninjaarena.gevel.data.EApiName
import com.ninjaarena.gevel.data.MarkerApi
import com.ninjaarena.gevel.framework.IApiSource
import com.ninjaarena.gevel.framework.flickr.FlickrSource
import com.ninjaarena.gevel.framework.foursquare.FoursquareSource
import com.ninjaarena.gevel.framework.googlePlaces.GooglePlacesSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MapViewModel(
    flickr: FlickrSource,
    googlePlaces: GooglePlacesSource,
    foursquare: FoursquareSource
) : ViewModel() {

    val liveData = MutableLiveData<ArrayList<MarkerApi>>()
    val trackPosition = MutableLiveData<Boolean>()
    val bottomInfo = MutableLiveData<Pair<Int, ArrayList<MarkerApi>>>()
    val markerDetail = MutableLiveData<MarkerApi>()
    val searchFilter = MutableLiveData<String?>()

    val sources: HashMap<EApiName, Api> = hashMapOf(
        EApiName.FLICKR to Api(flickr),
        EApiName.GOOGLE_PLACES to Api(googlePlaces),
        EApiName.FOUR_SQUARE to Api(foursquare)
    )

    fun searchPlaces(location: LatLng) {
        GlobalScope.launch(Dispatchers.Default) {
            updatePlaces()
            sources.forEach { _, u ->
                GlobalScope.launch(Dispatchers.Default) {
                    u.search(location, searchFilter.value)
                }
            }
        }
    }

    fun updatePlaces() {
        GlobalScope.launch(Dispatchers.Main) {
            val data = arrayListOf<MarkerApi>()
            sources.forEach { t, u -> data.addAll(u.getData()) }
            liveData.value = data
        }
    }

    fun updateDetail(marker: MarkerApi) {
        sources[marker.api]?.loadDetail(markerDetail)
    }

    class Api(private val source: IApiSource) : IApiSource {

        override fun loadDetail(markerDetail: MutableLiveData<MarkerApi>) {
            source.loadDetail(markerDetail)
        }

        override val livedata: MutableLiveData<ArrayList<MarkerApi>>
            get() = source.livedata

        val enabled = MutableLiveData<Boolean>()

        init {
            enabled.value = false
        }

        override fun getData(): ArrayList<MarkerApi> {
            return if (enabled.value == true)
                source.getData()
            else
                arrayListOf()
        }

        override fun search(location: LatLng, filter: String?) {
           if (enabled.value == true)
                source.search(location, filter)
        }

        fun setSelection(selected: Boolean) {
            enabled.value = selected
        }
    }

}