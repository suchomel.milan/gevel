package com.ninjaarena.gevel.ui

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.ninjaarena.gevel.R
import com.ninjaarena.gevel.data.EApiName
import com.ninjaarena.gevel.data.MarkerApi

class ClusterRenderer(context: Context, map: GoogleMap, clusterManager: ClusterManager<MarkerApi>) :
    DefaultClusterRenderer<MarkerApi>(context, map, clusterManager) {

    private val flickrMarker: BitmapDescriptor
    private val googlePlacesMarker: BitmapDescriptor
    private val foursquareMarker: BitmapDescriptor

    init {
        clusterManager.renderer = this
        flickrMarker = BitmapDescriptorFactory.fromResource(R.drawable.marker_flickr)
        googlePlacesMarker = BitmapDescriptorFactory.fromResource(R.drawable.marker_google)
        foursquareMarker = BitmapDescriptorFactory.fromResource(R.drawable.marker_foursquare)
    }

    override fun onBeforeClusterItemRendered(item: MarkerApi?, markerOptions: MarkerOptions?) {
        val icon = when (item?.api) {
            EApiName.FLICKR -> flickrMarker
            EApiName.GOOGLE_PLACES -> googlePlacesMarker
            EApiName.FOUR_SQUARE -> foursquareMarker
            else -> flickrMarker
        }

        markerOptions?.icon(icon)
        super.onBeforeClusterItemRendered(item, markerOptions)
    }
}