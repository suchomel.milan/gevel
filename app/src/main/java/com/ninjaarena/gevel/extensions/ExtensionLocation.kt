package com.ninjaarena.gevel.extensions

import com.google.android.gms.maps.model.LatLng

fun android.location.Location.toLatLng(): LatLng {
    return LatLng(this.latitude, longitude)
}

fun com.ninjaarena.gevel.framework.googlePlaces.data.nearby.Location.toLatLng(): LatLng {
    return LatLng(lat, lng)
}