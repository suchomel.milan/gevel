package com.ninjaarena.gevel.extensions

import timber.log.Timber

class ExtensionThread {

    companion object {
        fun LogCurrentThread() {
            Timber.d("Running on %s", Thread.currentThread().name)
        }
    }


}