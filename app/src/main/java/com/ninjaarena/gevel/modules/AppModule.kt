package com.ninjaarena.gevel.modules

import com.ninjaarena.gevel.framework.flickr.FlickrSource
import com.ninjaarena.gevel.framework.foursquare.FoursquareSource
import com.ninjaarena.gevel.framework.googlePlaces.GooglePlacesSource
import com.ninjaarena.gevel.ui.map.MapViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {

    viewModel { MapViewModel(flickr = get(), googlePlaces = get(), foursquare = get()) }

    single { FlickrSource() }

    single { GooglePlacesSource() }

    single { FoursquareSource() }

}