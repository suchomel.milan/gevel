package com.ninjaarena.gevel.data

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem


abstract class MarkerApi(
    val id: String,
    val latLng: LatLng,
    val api: EApiName
) : ClusterItem {

    override fun getPosition(): LatLng = latLng
    override fun getSnippet(): String = "Snippet"
    override fun getTitle(): String = "Title"

    override fun equals(other: Any?): Boolean {
        if (other is MarkerApi)
            return id == other.id && api == other.api
        return super.equals(other)
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + api.hashCode()
        return result
    }
}