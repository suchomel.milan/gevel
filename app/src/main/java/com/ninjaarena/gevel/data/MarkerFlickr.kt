package com.ninjaarena.gevel.data

import com.google.android.gms.maps.model.LatLng
import com.ninjaarena.gevel.framework.flickr.data.PhotoFlickr

class MarkerFlickr(val photo: PhotoFlickr) : MarkerApi(
    id = photo.id,
    latLng = LatLng(photo.latitude.toDouble(), photo.longitude.toDouble()),
    api = EApiName.FLICKR
)