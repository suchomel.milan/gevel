package com.ninjaarena.gevel.data

import com.ninjaarena.gevel.extensions.toLatLng

class MarkerGooglePlaces(val place: com.ninjaarena.gevel.framework.googlePlaces.data.nearby.Place) : MarkerApi(
    id = place.id,
    latLng = place.geometry.location.toLatLng(),
    api = EApiName.GOOGLE_PLACES
)