package com.ninjaarena.gevel.data

import com.ninjaarena.gevel.R
import java.security.InvalidParameterException

enum class EApiName(val resourceId: Int) {
    FLICKR(R.id.filterApiFlickr),
    GOOGLE_PLACES(R.id.filterApiGooglePlaces),
    FOUR_SQUARE(R.id.filterApiFoursquare);

    fun getFilterResourceId(): Int {
        return resourceId
    }

    companion object {
        fun valueOfResource(resourceId: Int): EApiName {
            return when (resourceId) {
                R.id.filterApiFlickr -> FLICKR
                R.id.filterApiGooglePlaces -> GOOGLE_PLACES
                R.id.filterApiFoursquare -> FOUR_SQUARE
                else -> throw InvalidParameterException()
            }
        }
    }
}