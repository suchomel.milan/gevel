package com.ninjaarena.gevel.data

import com.google.android.gms.maps.model.LatLng
import com.ninjaarena.gevel.framework.foursquare.data.VenueFoursquare

class MarkerFourSquare(val venue: VenueFoursquare) : MarkerApi(
    id = venue.id,
    latLng = LatLng(venue.location.lat, venue.location.lng),
    api = EApiName.FOUR_SQUARE
)